// Include the header file including the declaration of the QbitRegister class
// and of its methods.
#include "../qureg/qureg.hpp"

int main(int argc, char** argv){
  // Create the MPI environment, passing the same argument to all the ranks.
  openqu::mpi::Environment env(argc, argv);
  // qHiPSTER is structured so that only even number of ranks are used to store
  // and manipulate the quantum state. In case the number of ranks is not supported,
  // try to decrease it by 1 until it is.
  if (env.is_usefull_rank() == false) return 0;
  // qHiPSTER has functions that simplify some MPI instructions. However, it is important
  // to keep trace of the current rank.
  int myid = env.rank();


  int Nqubits = 4;
  
  //prepare register qubits.
  qubitReister<ComplexDP> QuReg_phi1(Nqubits, "basee", 0);

  //change Z to X, then apply hadamard gate.
  for (unsigned q=0, q<N, q++){
    QuReg_phi1.ApplyHadamard(q);
    QuReg_phi1.ApplyPauliX(q);
    QuReg_phi1.ApplyHadamard(q);
  }

  for(unsigned j=0, j<N, j++){
    if (myid == 0)
      {
          printf("probability that qubit %d is in state |1> is %g\n", j, QuReg_phi1.GetProbability(j));
      }
  }

} 
