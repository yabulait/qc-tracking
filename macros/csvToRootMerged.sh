#! bash

source progress_bar.sh

N=8

list_of_files=$(ls $1)

inputDir=$1
outputDir=./
if [ $# -gt 1 ]
then 
  outputDir=$2
fi

let _start=0
_end=$(echo "$list_of_files"| wc -l)
csv_to_root(){
  #if file not exist go to next file.
  if [ ! -f $inputDir/$ifile  ] 
  then 
    echo "Warning: file $ifile not exist!"
    continue
  fi

  #run conversion from csv to root
  root -l -b -q CsvToTtree.C\(\"$ifile\",\"$inputDir\",\"$outputDir\"\) > /dev/null 2>&1
  #echo "  "
  #check if the job is suceed.
  if [ "$?" == "0" ]
  then
    echo ""
    echo "$ifile --------------------------------- OK."
  else
    echo ""
    echo "$ifile --------------------------------- Fail."
  fi
}

#loop over all files and check if they exist.
for ifile in $list_of_files
do
  csv_to_root & pid=$!

  _start=$((_start+1))
  # allow only to execute $N jobs in parallel
  while [[ $(jobs -r -p | wc -l) -gt $N ]]
  do
        sleep 10
  done
  ProgressBar ${_start} ${_end}
done

#echo $pid
#wait for all
wait

ProgressBar ${_start} ${_end}

echo "all done!"
