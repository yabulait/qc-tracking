import sys
import os
import glob
import pandas as pd
import matplotlib.pyplot as plt
from array import array

import cppyy
from cppyy.gbl import std
from cppyy import double
import ROOT

def merge_csv(expr_str):
    #all_filenames = [i for i in glob.glob('{}'.format(expr_str))]
    all_filenames=["","","",""]
    all_filenames[0]=expr_str+"-cells.csv"
    all_filenames[1]=expr_str+"-hits.csv"
    all_filenames[2]=expr_str+"-truth.csv"
    all_filenames[3]=expr_str+"-particles.csv"
    dsl = [pd.read_csv(f) for f in all_filenames ]
    data = dsl[0]
    data = pd.merge(data, dsl[1],on='hit_id')
    data = pd.merge(data, dsl[2],on='hit_id')
    data = pd.merge(data, dsl[3],on='particle_id')
    
    data.weight.apply(func=float)
    data.vy.apply(func=float)
    return data

#fexpr="event000001001"
def write_to_csv(fexpr):
    result = merge_csv("../QC/Data/train_100_events/{}".format(fexpr))
    result.to_csv("../QC/Data/train_merged/{}.csv".format(fexpr),index=0)

## merge 0-100
def merge_hit_cell_particle_truth(begin=1000, end=1100):
    for i in range(begin,end,1):
        write_to_csv("event00000{}".format(i))

def get_vec(c_name):
    if c_name in ["hit_id", "ch0", "ch1","volume_id","layer_id","module_id","particle_id","q","nhits"]:
        return std.vector(int)()
    else:
        return std.vector(double)()
def fill_vec(pd_s, c_name):
    tmp_vec = get_vec(c_name)
    pd_s[c_name].map(tmp_vec.push_back)
    return tmp_vec
def fill_vec_all(pd_s):
    c_list = pd_s.columns.values
    return [(c_i,fill_vec(pd_s, c_i)) for c_i in c_list]

def fill_tree(tree, v_list):
    #tmp_tree = ROOT.TTree(tree_name, "all info of tracks")
    for c_name,v_c in v_list:
        tree.Branch(c_name, v_c)
    
    tree.Fill()
    #return tmp_tree
def fill_tree(tree, v_list, evt_id):
    #tmp_tree = ROOT.TTree(tree_name, "all info of tracks")
    evt_number = array( 'i', [ evt_id ] )
    tree.Branch("event_number", evt_number,"event_number/I")
    for c_name,v_c in v_list:
        tree.Branch(c_name, v_c)
    
    tree.Fill()
    #return tmp_tree

def write_to_tree(fexpr="event000001000.csv"):
    test_d = pd.read_csv("../QC/Data/train_merged/{}".format(fexpr))
    v_list = fill_vec_all(test_d)
    tfile = ROOT.TFile.Open("../QC/Data/tree/{0}.{1}".format(fexpr.strip(".csv"), "root"),"recreate")
    tfile.cd()
    mytree = ROOT.TTree("event", "all info of tracks")
    event_number= int( fexpr.strip(".csv").strip("event") )
    fill_tree(mytree, v_list, event_number)
    mytree.Write()
    tfile.Close()

