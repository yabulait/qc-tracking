# qc-tracking

## get data
 You can get data from here:
 [cern box](https://cernbox.cern.ch/index.php/s/yW7inwyQ4nfmdB0)
 
 Put thes data to qc-tracking/  
 you will get these data sets:
 
*  Data/test
*  Data/test_merged
*  Data/train_100_events
*  Data/train_merged
*  Data/tree

## CSV to tree  
100 events are converted into tree structure: Data/tree  

CSV files read by Pandas data frame in python.
1. merge the hit, cells, particles and truth in to one csv file, one file per event.  
2. convert each csv file in to root file with tree structure.

example code: [MergeCsvFiles.ipynb](MergeCsvFiles.ipynb)  